(in-package :cl-naive-sb-cover-ext)

(defun match-base-dir (base dir)
  (when (>= (length dir) (length base))
    (equal base (subseq dir 0 (length base)))))

(defun match-dirs (dirs path)
  (dolist (dir dirs)
    (when (match-base-dir (pathname-directory dir) (pathname-directory path))
      (return-from match-dirs t))))

(defun file-stats (file counts states source)
  (let ((summary (list :kind nil :covered nil :all nil :percentage nil))
        (stats (list :file nil
                     :info nil
                     :summaries nil
                     :source nil))
        (expression-coverage-percentage 0)
        (lines))

    (setf (getf stats :file) file)

    (when (zerop (sb-cover::all-of (getf counts :expression)))
      (setf (getf stats :info) :no-instrumented-forms)
      (return-from file-stats))

    (dolist (mode '(:expression :branch))
      (let ((count (getf counts mode))
            (stat-sum (copy-list summary)))

        (if (eql mode :expression)
            (setf expression-coverage-percentage (sb-cover::percent count)))

        (setf (getf stat-sum :kind) mode)
        (setf (getf stat-sum :covered) (sb-cover::ok-of count))
        (setf (getf stat-sum :all) (sb-cover::all-of count))
        (setf (getf stat-sum :percentage) (sb-cover::percent count))

        (push stat-sum (getf stats :summaries))))

    (with-input-from-string (stream source)
      (let ((no 0))
        (loop for ln = (read-line stream nil)
              while ln
              do (let ((line (list :no nil :state nil :src nil)))

                   (incf no)
                   (setf (getf line :no) no)
                   (setf (getf line :state) (elt states (- no 1)))
                   (setf (getf line :src) ln)
                   (push line lines)))
        (setf lines (reverse lines))
        (setf (getf stats :source)
              (list :no-lines no
                    :lines-covered (truncate
                                    (* no (/ expression-coverage-percentage 100)))
                    :lines lines))))

    stats))

(defun file-stats-sum (file)
  "Print a code coverage report of FILE into the stream HTML-STREAM."

  (let* ((source (sb-cover::detabify (sb-cover::read-file file :default)))
         (states (make-array (length source)
                             :initial-element 0
                             :element-type '(unsigned-byte 4)))
         (hashtable (sb-cover::code-coverage-hashtable))
         ;; Convert the code coverage records to a more suitable format
         ;; for this function.
         (expr-records (sb-cover::convert-records (gethash file hashtable) :expression))
         (branch-records (sb-cover::convert-records (gethash file hashtable) :branch))
         ;; Cache the source-maps
         (maps (with-input-from-string (stream source)
                 (loop with sb-cover::*current-package* = (find-package "CL-USER")
                       with map = nil
                       with form = nil
                       with eof = nil
                       for i from 0
                       do (setf (values form map)
                                (handler-case
                                    (sb-cover::read-and-record-source-map stream)
                                  (end-of-file ()
                                    (setf eof t))
                                  (error (error)
                                    (warn "Error when recording source map for toplevel form ~A:~%  ~A" i error)
                                    (values nil
                                            (make-hash-table)))))
                       until eof
                       when map
                       collect (cons form map)))))
    (mapcar (lambda (map)
              (maphash (lambda (k locations)
                         (declare (ignore k))
                         (dolist (location locations)
                           (destructuring-bind (start end suppress) location
                             (when suppress
                               (sb-cover::fill-with-state source states 15 (1- start)
                                                          end)))))
                       (cdr map)))
            maps)
    ;; Go through all records, find the matching source in the file,
    ;; and update STATES to contain the state of the record in the
    ;; indexes matching the source location. We do this in two stages:
    ;; the first stage records the character ranges, and the second stage
    ;; does the update, in order from shortest to longest ranges. This
    ;; ensures that for each index in STATES will reflect the state of
    ;; the innermost containing form.
    (let ((counts (list :branch (make-instance 'sb-cover::sample-count :mode :branch)
                        :expression (make-instance 'sb-cover::sample-count
                                                   :mode :expression))))
      (let ((records (append branch-records expr-records))
            (locations nil))
        (dolist (record records)
          (destructuring-bind (mode path state) record
            (let* ((path (reverse path))
                   (tlf (car path))
                   (source-form (car (nth tlf maps)))
                   (source-map (cdr (nth tlf maps)))
                   (source-path (cdr path)))
              (cond ((eql mode :branch)
                     (let ((count (getf counts :branch)))
                       ;; For branches mode each record accounts for two paths
                       (incf (sb-cover::ok-of count)
                             (ecase state
                               (5 2)
                               ((6 9) 1)
                               (10 0)))
                       (incf (sb-cover::all-of count) 2)))
                    (t
                     (let ((count (getf counts :expression)))
                       (when (eql state 1)
                         (incf (sb-cover::ok-of count)))
                       (incf (sb-cover::all-of count)))))
              (if source-map
                  (handler-case
                      (multiple-value-bind (start end)
                          (sb-cover::source-path-source-position (cons 0 source-path)
                                                                 source-form
                                                                 source-map)
                        (push (list start end source state) locations))
                    (error ()
                      (warn "Error finding source location for source path ~A in file ~A~%" source-path file)))
                  (warn "Unable to find a source map for toplevel form ~A in file ~A~%" tlf file)))))
        ;; Now process the locations, from the shortest range to the longest
        ;; one. If two locations have the same range, the one with the higher
        ;; state takes precedence. The latter condition ensures that if
        ;; there are both normal- and a branch-states for the same form,
        ;; the branch-state will be used.
        (setf locations (sort locations #'> :key #'fourth))
        (dolist (location (stable-sort locations #'<
                                       :key (lambda (location)
                                              (- (second location)
                                                 (first location)))))
          (destructuring-bind (start end source state) location
            (sb-cover::fill-with-state source states state start end))))

      (list
       :states states
       :file-stats (file-stats file counts states source)))))

(defun report-stats (base-directory &key (include-extentions '("lisp"))
                     include-directories)
  (let ((paths)
        (if-matches 'identity)
        (base (if (stringp base-directory)
                  (pathname-directory base-directory)
                  base-directory)))
    (maphash (lambda (k v)
               (declare (ignore v))
               (when (match-base-dir base (pathname-directory k))
                 (when (or (not include-directories)
                           (match-dirs include-directories k))
                   (when (member (pathname-type k) include-extentions   :test #'equalp)

                     (when (funcall if-matches k)
                       (let* ((pk (translate-logical-pathname k))
                              (n (format nil "~(~{~2,'0X~}~)"
                                         (coerce (sb-md5:md5sum-string
                                                  (sb-ext:native-namestring pk))
                                                 'list))))

                         (when (probe-file k)
                           (push (list* :file k :uuid n (file-stats-sum k))
                                 paths))))))))
             (sb-cover::code-coverage-hashtable))
    (setf paths (sort paths #'string< :key #'(lambda (path)
                                               (getf path :file))))))

(defun report-ext (directory &key
                   ((:form-mode sb-cover::*source-path-mode*) :whole)
                   (if-matches 'identity)
                   (external-format :default)
                   base-directory
                   (include-directories)
                   (include-extentions '("lisp")))
  "Print a code coverage report of all instrumented files into DIRECTORY.
If DIRECTORY does not exist, it will be created. The main report will be
printed to the file cover-index.html. The external format of the source
files can be specified with the EXTERNAL-FORMAT parameter.

If the keyword argument FORM-MODE has the value :CAR, the annotations in
the coverage report will be placed on the CARs of any cons-forms, while if
it has the value :WHOLE the whole form will be annotated (the default).
The former mode shows explicitly which forms were instrumented, while the
latter mode is generally easier to read.

The keyword argument IF-MATCHES should be a designator for a function
of one argument, called for the namestring of each file with code
coverage info. If it returns true, the file's info is included in the
report, otherwise ignored. The default value is CL:IDENTITY.
"
  (let* ((paths)
         (base (if (stringp base-directory)
                   (pathname-directory base-directory)
                   base-directory))
         (directory (sb-cover::pathname-as-directory directory))
         (defaults (translate-logical-pathname directory)))
    (ensure-directories-exist defaults)
    (when (eq if-matches 'identity)
      (sb-cover::refresh-coverage-info)) ; update all
    (maphash (lambda (k v)
               (declare (ignore v))

               (when (match-base-dir base (pathname-directory k))
                 (when (or (not include-directories)
                           (match-dirs include-directories k))
                   (when (member (pathname-type k) include-extentions   :test #'equalp)

                     (when (funcall if-matches k)
                       (unless (eq if-matches 'identity)
                         (sb-cover::refresh-coverage-info k)) ; update one file
                       (let* ((pk (translate-logical-pathname k))
                              (n (format nil "~(~{~2,'0X~}~)"
                                         (coerce (sb-md5:md5sum-string
                                                  (sb-ext:native-namestring pk))
                                                 'list))))
                         (when (probe-file k)
                           (with-open-file (stream (make-pathname :name n :type "html"
                                                                  :defaults directory)
                                                   :direction :output
                                                   :if-exists :supersede
                                                   :if-does-not-exist :create)
                             (push (list* k n (sb-cover::report-file
                                               k
                                               stream external-format))
                                   paths)))))))))
             (sb-cover::code-coverage-hashtable))
    (let ((report-file (make-pathname :name "cover-index" :type "html" :defaults directory)))
      (with-open-file (stream report-file
                              :direction :output :if-exists :supersede
                              :if-does-not-exist :create)
        (sb-cover::write-styles stream)
        (unless paths
          (warn "No coverage data found for any file, producing an empty report. Maybe you~%forgot to (DECLAIM (OPTIMIZE SB-COVER:STORE-COVERAGE-DATA))?")
          (format stream "<h3>No code coverage data found.</h3>")
          (close stream)
          (return-from report-ext))
        (format stream "<table class='summary'>")
        (format stream "<tr class='head-row'><td></td><td class='main-head' colspan='3'>Expression</td><td class='main-head' colspan='3'>Branch</td></tr>")
        (format stream "<tr class='head-row'>~{<td width='80px'>~A</td>~}</tr>"
                (list "Source file"
                      "Covered" "Total" "%"
                      "Covered" "Total" "%"))
        (setf paths (sort paths #'string< :key #'car))
        (loop for prev = nil then source-file
              for (source-file report-file expression branch) in paths
              for even = nil then (not even)
              do (when (or (null prev)
                           (not (equal (pathname-directory (pathname source-file))
                                       (pathname-directory (pathname prev)))))
                   (format stream "<tr class='subheading'><td colspan='7'>~A</td></tr>~%"
                           (namestring (make-pathname :directory (pathname-directory (pathname source-file))))))
              do (format stream "<tr class='~:[odd~;even~]'><td class='text-cell'><a href='~a.html'>~a</a></td>~{<td>~:[-~;~:*~a~]</td><td>~:[-~;~:*~a~]</td><td>~:[-~;~:*~5,1f~]</td>~}</tr>"
                         even
                         report-file
                         (enough-namestring (pathname source-file)
                                            (pathname source-file))
                         (list (sb-cover::ok-of expression)
                               (sb-cover::all-of expression)
                               (sb-cover::percent expression)
                               (sb-cover::ok-of branch)
                               (sb-cover::all-of branch)
                               (sb-cover::percent branch))))
        (format stream "</table>"))
      report-file)))

(defun summary-report (paths)
  (loop for path in paths
        collect
        (let ((expression)
              (branch))
          (dolist (sum (getf (getf path :file-stats) :summaries))
            (if (eql (getf sum :kind) :branch)
                (setf branch (subseq sum 2))
                (setf expression (subseq sum 2))))

          (list :file (getf path :file)
                :no-lines (getf (getf (getf path :file-stats) :source) :no-lines)
                :lines-covered (getf (getf (getf path :file-stats) :source)
                                     :lines-covered)
                :expression expression
                :branch branch))))

(defun get-unix-epoch-time ()
  "Get the current time as UNIX epoch time."
  (let* ((seconds-from-1900-to-1970 (* 70 365 24 60 60))
         (leap-days (- 1970 1900 3))
         (leap-seconds (* leap-days 24 60 60))
         (current-time (get-universal-time)))
    (- current-time seconds-from-1900-to-1970 leap-seconds)))

(defun gitlab-reports (summary-report)
  (let ((no-lines 0)
        (lines-covered 0)
        (expression-covered 0)
        (expression-all 0)
        ;; (expression-% 0)
        (branch-covered 0)
        (branch-all 0)
        ;;  (branch-% 0)
        (expression-rate 0)
        (branch-rate 0)
        (packages))

    (dolist (sum summary-report)

      (incf expression-covered (getf (getf sum :expression) :covered))
      (incf expression-all (getf (getf sum :expression) :all))
      (incf branch-covered (getf (getf sum :branch) :covered))
      (incf branch-all (getf (getf sum :branch) :all))
      (incf no-lines (getf sum :no-lines))
      (incf lines-covered (getf sum :lines-covered))

      (push (format nil "<class name=\"~A\" filename=\"~A\" line-rate=\"~4,2F\" branch-rate=\"~4,2F\" complexity=\"~4,2F\" ></class>"
                    (pathname-name (getf sum :file))
                    (str:replace-first "/home/phil/source/naive/cl-naive-store/"
                                       ""
                                       (getf sum :file))
                    (if (and (> (getf (getf sum :expression) :covered) 0)
                             (> (getf (getf sum :expression) :all) 0))
                        (/ (getf (getf sum :expression) :covered)
                           (getf (getf sum :expression) :all))
                        0)
                    (if (and (> (getf (getf sum :branch) :covered) 0)
                             (> (getf (getf sum :branch) :all) 0))
                        (/ (getf (getf sum :branch) :covered)
                           (getf (getf sum :branch) :all))
                        0)
                    (if (and (> (getf (getf sum :branch) :all) 0)
                             (> (getf (getf sum :expression) :all) 0))
                        (/ (getf (getf sum :branch) :all)
                           (getf (getf sum :expression) :all))
                        0))
            packages))

    (when (and (> expression-covered 0)
               (> expression-all 0))
      (setf expression-rate (/ expression-covered expression-all)))

    (when (and (> branch-covered 0)
               (> branch-all 0))
      (setf branch-rate (/ branch-covered branch-all)))

    (list
     :summary (format nil
                      "~%~%coverage-stats:
  line-rate: ~4,2F%
  branch-rate: ~4,2F%
  expression-rate: ~4,2F%~%~%"
                      (* expression-rate 100)
                      (* branch-rate 100)
                      (* expression-rate 100))
     :cobertura
     (format nil
             "<coverage line-rate=\"~4,2F\" branch-rate=\"~4,2F\"
  expression-rate=\"~4,2F\"
  lines-covered=\"~4,2F\"
  lines-valid=\"~4,2F\" branches-covered=\"~A\" branches-valid=\"~A\"
  expressions-covered=\"~4,2F\" expressions-valid=\"~4,2F\"
  complexity=\"~4,2F\" version=\"0\" timestamp=\"~A\">
<sources>
</sources>
<packages>
<package name=\"cl-naive-store\" line-rate=\"0.67\" branch-rate=\"0\" complexity=\"0\">
<classes>

~%~{~a~%~}~%</classes>~%</package>~%</packages>~%</coverage>"
             expression-rate
             branch-rate
             expression-rate
             lines-covered
             no-lines
             branch-covered
             branch-all
             expression-covered
             expression-all
             (if (and (> branch-all 0)
                      (> expression-all 0))
                 (/ branch-all expression-all)
                 0)
             (get-unix-epoch-time)
             (reverse packages)))))

(defun save-gitlab-reports (reports &optional
                            (directory (cl-fad::get-default-temporary-directory)))

  (let ((summary-file (cl-fad:merge-pathnames-as-file
                       directory
                       (make-pathname
                        :defaults directory
                        :name "coverage-summary"
                        :type "txt"
                        :version nil)))
        (cobertura-file (cl-fad:merge-pathnames-as-file
                         directory
                         (make-pathname
                          :defaults directory
                          :name "coverage"
                          :type "xml"
                          :version nil))))

    (ensure-directories-exist summary-file)

    (with-open-file (stream summary-file :direction :output
                                         :if-exists :supersede
                                         :if-does-not-exist :create)
      (format stream "~A" (getf reports :summary)))

    (with-open-file (stream cobertura-file :direction :output
                                           :if-exists :supersede
                                           :if-does-not-exist :create)
      (format stream "~A" (getf reports :cobertura)))))

(defmacro with-sb-cover ((system) &body body)
  `(progn
   ;;; Load SB-COVER
     (require :sb-cover)

     ;; Turn on generation of code coverage instrumentation in the compiler
     (declaim (optimize sb-cover:store-coverage-data))

     ;; Load some code, ensuring that it's recompiled with the new optimization
     ;; policy.
     (asdf:oos 'asdf:load-op ,system :force t)

     ,@body

     ;; Turn off instrumentation
     (declaim (optimize (sb-cover:store-coverage-data 0)))))
