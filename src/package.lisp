(in-package :common-lisp-user)

(defpackage :cl-naive-sb-cover-ext
  (:use :cl :sb-cover)
  (:export

   :report-stats
   :summary-report
   :gitlab-reports
   :save-gitlab-reports
   :report-ext
   :with-sb-cover))
