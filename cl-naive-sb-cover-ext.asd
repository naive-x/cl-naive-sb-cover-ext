(defsystem "cl-naive-sb-cover-ext"
  :description ""
  :version "2024.1.23"
  :author ""
  :licence "MIT"
  :depends-on (:sb-cover :str :cl-fad)
  :components ((:file "src/package")
               (:file "src/coverage" :depends-on ("src/package"))))

